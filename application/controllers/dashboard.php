<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
session_start();

class Dashboard extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
   
	function __construct()
	{
		parent::__construct();
	}
	
    public function index() {
    	
    	//Valida a Sessao da Dashboard
    	$url = "dashboard";
    	$this->validaSessao($url);
    	
    }

    public function login() {
        
        $this->load->view('includes/html_header');
        $this->load->view('login');
        $this->load->view('includes/html_footer');
    }
    
    public function validaSessao($url){
    	if($this->session->userdata('usuarioLogado')){
    		$session_data = $this->session->userdata('usuarioLogado');
    		$data['usuario'] = $session_data['nome'];
    		$this->load->view('includes/html_header');
    		$this->load->view('includes/menu');
    		$this->load->view($url, $data);
    		$this->load->view('includes/html_footer');
    	}else {
    		redirect('dashboard/login');
    	}
    }
    
    public function logar(){
    	//$logado = 0;
        $usuario = $this->input->post('usuario');
        $senha = $this->input->post('senha');
        
        $this->db->where('usuario',$usuario);
        $this->db->where('senha',$senha);
        $this->db->where('status',1);
        $data['usuario'] = $this->db->get('usuario')->result();
        
        if(count($data['usuario'])==1){
            $dados['nome'] = $data['usuario'][0]->nome;
            $dados['id'] = $data['usuario'][0]->idUsuario;
            $dados['logado'] = true;
            $this->session->set_userdata('usuarioLogado',$dados);
            redirect('dashboard');
        }else{
            redirect('dashboard/login');
        }
        
        //return $logado;
    }
    
    function check_database($password)
    {
    	//Field validation succeeded.  Validate against database
    	$username = $this->input->post('usuario');
    
    	//query the database
    	$result = $this->usuario->login($username, $password);
    
    	if($result)
    	{
    		$sess_array = array();
    		foreach($result as $row)
    		{
    			$sess_array = array(
    					'id' => $row->id,
    					'usuario' => $row->usuario
    			);
    			$this->session->set_userdata('logged_in', $sess_array);
    		}
    		return TRUE;
    	}
    	else
    	{
    		$this->form_validation->set_message('check_database', 'Invalid username or password');
    		return false;
    	}
    }
    
    public function logout(){
        $this->session->unset_userdata('usuarioLogado');
        redirect('dashboard/login');
    }
    
}




    /* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */    