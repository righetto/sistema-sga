<?php $id_cliente = $this->uri->segment(3); ?>

<script src="http://code.jquery.com/jquery-1.7.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="col-md-10">
            <h1>
                Cadastro
                <small>de novo Aluno</small>
            </h1>
        </div>

        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Dados</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form action="<?= base_url() ?>aluno/cadastrar" method="post">
                    <div class="box-body">
                        <form class="form-control">
                            <div class="form-group">
                                <label for="nome">Nome do Aluno:</label>
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe o nome..." required>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="dataNascimento">Data Nascimento:</label>
                                    <input type="date" class="form-control" id="dataNascimento" name="dataNascimento" required>
                                </div>
                                <div class="col-md-3">
                                    <label for="escola">Escola:</label>
                                    
                                    <select id="escola" class="form-control" name="escola" required>
                                        <option value="0">Selecione ...</opton>
                                        <option value=""></opton>
                                        <option value=""></opton>                                        
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="professor">Professor:</label>
                                    <input type="text" class="form-control" id="professor" name="professor" placeholder="Informe o nome..."required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="sala">Sala:</label>
                                    <input type="text" class="form-control" id="sala" name="sala" placeholder="Informe a sala..." required>
                                </div>
                                <div class="col-md-3">
                                    <label for="serie">Serie:</label>
                                    <input type="text" class="form-control" id="serie" name="serie" placeholder="Informe a serie..." required>
                                </div>
                                <div class="col-md-3">
                                    <label for="horarioEntrada">Horario Entrada:</label>
                                    <input type="text" class="form-control" id="horarioEntrada" name="horarioEntrada" placeholder="Informe o horario..." required>
                                </div>
                                <div class="col-md-3">
                                    <label for="horarioSaida">Horario Saída:</label>
                                    <input type="text" class="form-control" id="horarioSaida" name="horarioSaida" placeholder="Informe o horario..." required>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4">
                                    <label for="mendalidae">Mensalidade:</label>
                                    <input type="text" class="form-control" id="mensalidade" name="mensalidade" placeholder="Informe a mensalidade..." required>
                                </div>
                                <div class="col-md-4">
                                    <label for="inicioContrato">Início Contrato:</label>
                                    <input type="date" class="form-control" id="inicioContrato" name="inicioContrato" required>
                                </div>
                                <div class="col-md-4">
                                    <label for="terminoContrato">Término de Contrato:</label>
                                    <input type="date" class="form-control" id="terminoContrato" name="terminoContato" required>
                                </div>
                            </div>

                            <div style="text-align: right">
                                </br>
                                <button type="submit" class="btn btn-primary">Cadastrar</button>
                                <a href="<?= base_url() ?>dashboard" class="btn btn-default" type="reset">Cancelar</a>
                            </div>


                        </form>
                    </div><!-- /.box -->
                    

            </div>


    </section>


</div><!-- /.content-wrapper -->
<footer class="main-footer">

<script type="text/javascript">$(function(){
	$("#cpf").mask('000.000.000-00');
	$("#cep").mask('00000-000');
	$("#uf").mask('AA');
	$("#telefoneRes").mask('(00) 0000-0000');
	$("#telefoneCel").mask('(00) 0000-00009');
	$("#telComercial").mask('(00) 0000-0000');
	$("#horarioEntrada").mask('00:00:00');
	$("#horarioSaida").mask('00:00:00');
	$('#mensalidade').mask('000.000.000.000.000,00', {reverse: true});
});
</script>

    <strong>Copyright &copy; 2014-2015 Equipe OPE</a>.</strong> Todos os direitos reservados.
</footer>

