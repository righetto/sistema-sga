create database sistema;

use sistema;

-- TABELA DE USUARIO
create table usuario
(
	idUsuario int auto_increment primary key not null,
	nome varchar(100) not null,
	usuario varchar(20) not null,
	senha varchar(15) not null,
	status int not null,
	nivel int not null
);

-- TABELA ESCOLA
Create table escola(
	idEscola int auto_increment primary key not null,
	nome varchar(50) not null,
	nomeDiretor varchar(50) not null,
	endereco varchar(100) not null,
	bairro varchar(50) not null,
	telefone varchar(15) not null,
	email varchar(30) not null
);
-- TABELA VEICULO
Create table veiculo(
	idVeiculo int auto_increment primary key not null,
	modelo varchar(20) not null,
	placa varchar(10) not null,
	ano varchar(10) not null,
	seguro varchar(10) not null
);
-- TABELA FUNCIONARIO
Create table funcionario(
	idFuncionario int auto_increment primary key not null,
	nome varchar(100) not null,
	cpf varchar(20) not null,
	rg varchar(15) not null,
	dataNascimento date not null,
	sexo char(1) not null,
	cep varchar(15) not null,
	endereco varchar(100) not null,
	bairro varchar(50) not null,
	cidade varchar(50) not null,
	uf char(2) not null,
	telefoneRes varchar(15) not null,
	telefoneCel varchar(15) not null,
	cargo varchar(20) not null,
	dataAdmissao date not null,
	status varchar(20) not null
);
-- TABELA CLIENTE
Create table cliente(
	idCliente int auto_increment primary key not null,
	nome varchar(100) not null,
	cpf varchar(20) not null,
	rg varchar(15) not null,
	dataNascimento date not null,
	sexo char(1) not null,
	cep varchar(15) not null,
	endereco varchar(100) not null,
	numero char(5) not null,
	bairro varchar(50) not null,
	cidade varchar(50) not null,
	uf char(2) not null,
	telefoneRes varchar(15) not null,
	telefoneCel varchar(15) not null,
	telComercial varchar(15) not null
);
-- TABELA ITINERARIO
Create table itinerario(
	idItinerario int auto_increment primary key not null,
	itinerario char(2) not null,
	horario_Part_Ida char(5) not null,
	horario_Cheg_Ida char(5) not null,
	horario_Part_Volta char(5) not null,
	horario_Cheg_Volta char(5) not null,
	motorista varchar(20) not null,
	monitor varchar(20) not null
);
-- TABELA ALUNO --
Create table aluno(
	idAluno int auto_increment primary key not null,
	nome varchar(200) not null,
        dataNascimento date not null,
        escola varchar(20) not null,
        professor varchar(70) not null,
        sala varchar(10) not null,
        serie varchar(10) not null,
        horarioEntrada varchar(10) not null,
        horarioSaida varchar(10) not null,
        dataVencimento varchar(10) not null,
        mensalidade varchar(10) not null,
        inicioContrato date not null,
        terminoContrato date not null,
	cli_aluno int not null,
	constraint fk_aluno_cliente FOREIGN KEY (cli_aluno)  REFERENCES cliente (idCliente)
);
